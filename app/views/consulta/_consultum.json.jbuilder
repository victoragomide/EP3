json.extract! consultum, :id, :motivo, :dia, :horario, :created_at, :updated_at
json.url consultum_url(consultum, format: :json)
