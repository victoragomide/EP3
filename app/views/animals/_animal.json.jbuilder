json.extract! animal, :id, :nome, :nascimento, :sexo, :tipo, :pelagem, :raca, :foto, :created_at, :updated_at
json.url animal_url(animal, format: :json)
