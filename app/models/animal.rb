class Animal < ApplicationRecord

  belongs_to :user
  has_many :consulta

  belongs_to :tipo
  belongs_to :raca

  validates_presence_of :nome

  mount_uploader :foto, PictureUploader

end
