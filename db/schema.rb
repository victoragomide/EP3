# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170704054027) do

  create_table "animals", force: :cascade do |t|
    t.string   "nome"
    t.date     "nascimento"
    t.string   "sexo"
    t.string   "tipo"
    t.string   "pelagem"
    t.string   "raca"
    t.string   "foto"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "tipo_id"
    t.integer  "raca_id"
    t.integer  "user_id"
    t.index ["raca_id"], name: "index_animals_on_raca_id"
    t.index ["tipo_id"], name: "index_animals_on_tipo_id"
    t.index ["user_id"], name: "index_animals_on_user_id"
  end

  create_table "consulta", force: :cascade do |t|
    t.string   "motivo"
    t.date     "dia"
    t.time     "horario"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.integer  "animal_id"
    t.index ["animal_id"], name: "index_consulta_on_animal_id"
    t.index ["user_id"], name: "index_consulta_on_user_id"
  end

  create_table "racas", force: :cascade do |t|
    t.string   "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipos", force: :cascade do |t|
    t.string   "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
