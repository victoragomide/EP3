class AddUserToConsulta < ActiveRecord::Migration[5.0]
  def change
    add_reference :consulta, :user, foreign_key: true
  end
end
