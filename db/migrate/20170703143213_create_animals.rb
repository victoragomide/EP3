class CreateAnimals < ActiveRecord::Migration[5.0]
  def change
    create_table :animals do |t|
      t.string :nome
      t.date :nascimento
      t.string :sexo
      t.string :pelagem
      t.string :foto

      t.timestamps
    end
  end
end
