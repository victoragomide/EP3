class AddAnimalToConsulta < ActiveRecord::Migration[5.0]
  def change
    add_reference :consulta, :animal, foreign_key: true
  end
end
