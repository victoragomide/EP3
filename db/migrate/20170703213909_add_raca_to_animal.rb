class AddRacaToAnimal < ActiveRecord::Migration[5.0]
  def change
    add_reference :animals, :raca, foreign_key: true
  end
end
