class CreateConsulta < ActiveRecord::Migration[5.0]
  def change
    create_table :consulta do |t|
      t.string :motivo
      t.date :dia
      t.time :horario

      t.timestamps
    end
  end
end
