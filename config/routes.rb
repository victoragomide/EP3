Rails.application.routes.draw do
  resources :consulta
  resources :tipos
  resources :racas
  devise_for :users
  root to: redirect('/animals')
  resources :animals
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
